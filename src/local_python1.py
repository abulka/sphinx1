"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""

class Fred:
    pass

class Mary:
    pass

class BigSam:
    """
    This is a docstring yes.  But can we do sphinx stuff in here?

    .. note::
        There are many other Info fields but they may be redundant:
            * param, parameter, arg, argument, key, keyword: Description of a
              parameter.

    Well that's nice.
    """

    def do_something(self, how, many, times):
        """
        This does something

        :param how: how to do it
        :param many: many is just silly
        :param times: and this is times by the number of times  by the number of times  by the number of times !
        :return: None
        """
        pass

    def calc(self):
        """
        Does something complex. :-)

        :return: 1 + 1

        .. uml::

            @startuml

            title testing plantml here

            a-b
            a-c
            a..>d

            class d {
                a
                fearless
                setof
                attributes
                and()
                methods()
            }

            @enduml

        And here is another uml diagram

        .. uml::

            @startuml

            class Thing {
                name
                address
                __init__()
            }


            class Calculator {
                things
                compute()
                add()
            }

            Calculator *--> "*" Thing : things

            class ThisClass {
                static1
                static2
                calculator
                hello()
                calc()
            }

            ThisClass ..> Calculator : calculator

            Substance <|- Thing
            Substance <|- Nuclear


            note left of ThisClass
              This class is the start, <size:18>of our thinking</size> about this code
              it <b>uses</b> a <i>Calculator</i> class which in <u>turn</u>
              uses many <size:18>thing</size> objects. :-)
            end note

            @enduml

        """