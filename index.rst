.. sphinx1 documentation master file, created by
   sphinx-quickstart on Wed Mar 16 15:38:02 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome
=======

Contents:

.. toctree::
   :maxdepth: 1

   README <README.md>
   andy
   Other <other.rst>
   pynsource
   marky.md
   plantweb/plantweb.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
