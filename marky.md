# Markdown test

I hope markdown is being supported.
**bold** and _italic_ and
and a url <http://blog.readthedocs.com/adding-markdown-support/> should all be working.


Single * means italic and double asterix means bold.  For example *hi* this is a test and **another** emphasis.

## Lists
(In this example, leading and trailing spaces are shown with with dots: ⋅)

1. First ordered list item
2. Another item
    * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
    1. Ordered sub-list
4. And another item.

    You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

    To have a line break without a paragraph, you will need to use two trailing spaces.  
    Note that this line is separate, but within the same paragraph.  
    (This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses

## Code and Syntax Highlighting

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```
 
```python
s = "Python syntax highlighting"
print s
```
 
```
No language indicated, so no syntax highlighting. 
But let's throw in a <b>tag</b>.
```

Readme.md inclusion attempt
------------------------

Won't work here because this file is pure markdown and not a .rst where you can have .. directives
