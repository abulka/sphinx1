Other Stuff
=================

this is the single page model

.. automodule:: singlepage.models
    :members:
    :undoc-members:


How to get more sense from this class?

See https://djangosnippets.org/snippets/2533/
**DONE!**


Here is some uml
----------------

.. uml::

    @startuml

    skin BlueModern
    title testing plantml here
    box "CDCI Orchestration"
    participant "CDCI" as A
    participant "Billing Adapter" as B
    participant "OBA" as C
    end box

    activate A
    A -> B: "GET /parallels/resources/v1/<uuid>"
    B -> C: GetOrder_API(OrderID)
    B <-- C: &#91;&#91; ..., "CP", ... &#93;&#93;

    A <-- B: "HTTP/1.1 code 200"
    deactivate A

    @enduml

well I hope the diagram above was generated.

A note I think
--------------

.. note::
    There are many other Info fields but they may be redundant:
        * param, parameter, arg, argument, key, keyword: Description of a
          parameter.
        * type: Type of a parameter.
        * raises, raise, except, exception: That (and when) a specific
          exception is raised.
        * var, ivar, cvar: Description of a variable.
        * returns, return: Description of the return value.
        * rtype: Return type.

My local python file
--------------------

.. automodule:: local_python1
    :members:
    :undoc-members:

Markdown Support
----------------

Won't work in this file because its `.rst` format.

Also, that's why including a README.md in here will not work, because its just .md injected into
a .rst file.  Thus don't bother trying::

    .. include:: ./README.md

To enable Markdown Support you need to edit the conf.py file with the following:

.. literalinclude:: ./conf.py
    :linenos:
    :language: python
    :lines: 19-24, 66-67
    :lineno-start: 19

