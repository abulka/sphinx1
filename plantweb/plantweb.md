# Plantweb

Plantweb is a python library and command line, that invokes 
the __remote plantUML server__.  

This means you don't need to install
and run a local java plantuml.jar when generating documentation
 using Sphinx.  
 - Handy if you can't be bothered setting that up
 and have online access.  
 - Handy if you are hosting on readthedocs.org thus
 may not have the ability to run java bits and pieces there
 
## Thoughts

1. Being able to run plantuml from a jar file locally
is something that sphinx does, and is therefore something
that GitUML should be able to do.  
    - Heroku would need to support this in production too.
    - Would make GitUml independent of PlantUml
    
2. Being able to use the remote PlantUml server via a nicer
interface (plantweb) may be useful to GitUml in its current form.

