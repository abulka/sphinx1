Andy
====

This *is* a test

The :py:func:`enumerate` function is described below.

* This is a bulleted list.
* It has two items, the second
  item uses two lines.

1. This is a numbered list.
2. It has two items too.

#. This is a numbered list.
#. It has two items too.


* this is
* a list

  * with a nested list
  * and some subitems

* and here the parent list continues

term (up to a line of text)
   Definition of the term, which must be indented

   and can even consist of multiple paragraphs

next term
   Description.

| These lines are
| broken exactly like in
| the source file.

This is a normal text paragraph. The next paragraph is a code sample::

   It is not processed in any way, except
   that the indentation is removed.

   It can span multiple lines.

This is a normal text paragraph again.

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======

This is a paragraph that contains `a link`_.

.. _a link: http://example.com/

orchestration png
-----------------

.. image:: _static/orchestration.png


hello

.. py:function:: enumerate(sequence[, start=0])

   Return an iterator that yields tuples of an index and an item of the
   *sequence*. (And so on.)

.. py:class:: Base(object)

   Is a cool class.

.. figure:: _static/orchestration.png
   :align: center
   :alt: CDCI orchestration


singlepage.views
----------------

.. automodule:: singlepage.views
  :members:
  :undoc-members:


Individual function
-------------------

.. autofunction:: lib.python_to_image_url.python_to_image_url

.. autofunction:: singlepage.views.diagram_list



