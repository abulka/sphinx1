# Sphinx Documentation Playground

Mucking around and learning sphinx.

## Installation

This was originally a _python 3_ project.
There is no problem running this as a _python 2_ project.  On both Macbook Air and iMac, 
running as python 2 is OK because django is installed in my
__py2k__ environments.


Though I am getting an error

    from parsing.common.gen_plantuml import PythonToPlantUml
    ImportError: No module named common.gen_plantuml

not sure why.  Ignore this for now.

## References
This project references both 
- the django project gituml (python 2 project) in a sibling directory 
- as well as referencing pynsource (python 2 project) in a sibling directory.

which makes it problematic to deploy to _readthedocs_ since those directories are not present.
Also, I would need to use plantweb in order to render the UML diagrams.  

## TODO
I need to create a new project which:

- doesn't do any sibling directory referencing
- is a django project itself
- uses the __doc__ subdir for sphinx doco
- Uses plantweb in order to render the UML diagrams
- Uses nice google style docstrings (needs a plugin to convert and process properly in sphinx)
- Parses javascript
- uses webhooks from bitbucket to readthedocs
 
## Tips

Remember to `pip install -r requirements.txt`

Watch out `conf.py` refers to other directories, its like I'm 
experimenting with documenting multiple projects - and sucking
them into the one project.  Thus there are other module dependencies
that kick in e.g. `import dj_database_url` or whatever it is

## Run

```
make html
```

will create a `_build/html` directory and all files in there.

## Sphinx Doco

Intro tutorial <http://www.sphinx-doc.org/en/stable/tutorial.html>

Read the docs <https://readthedocs.org/> wow - it is a hosting site for 
documentation and looks like what I want to do with gituml. 
It even responds to hooks in GIT and knows when to regenerate the 
documentation! 
It also has info on how to incorporate __markdown__ files into the doco,
which I am already doing locally.

Markdown cheatsheet <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links>

Sphinx .rst cheatsheet <http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html#text-syntax-bold-italic-verbatim-and-special-characters>



